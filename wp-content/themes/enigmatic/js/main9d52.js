jQuery.noConflict();

jQuery(document).ready(function ($) {

    function bool_of(bool_string) {
        var bool_value = ((bool_string == "true" || bool_string == "1") ? true : false);
        return bool_value;
    }


    /* Delay the display of the menu until all elements are built 
     $('#menu-primary-menu').css('visibility', 'visible');
     */


    /* ---------------------------------- Drop-down Menu.-------------------------- */


    $('ul.menu').superfish({
        delay: 100, // one second delay on mouseout
        animation: {height: 'show'}, // fade-in and slide-down animation
        speed: 'fast', // faster animation speed
        autoArrows: false                           // disable generation of arrow mark-up
    });

    $('.responsive-select-menu').change(function () {
        window.location = $(this).find('option:selected').val();
    });

    /*--------------------------------------------------
     Home Page
     ---------------------------------------------------*/
    /*-----------------------------------------------------------------------------------*/
    /*	Toggle Portfolio and Blog Showcase for the Homepage
     /*-----------------------------------------------------------------------------------*/

    $('#blog-tab a').click(function () {

        $('#portfolio-showcase').fadeOut(400, function () {
            $('#blog-showcase').fadeIn(400);
        })

        $('#project-tab').removeClass('current-tab');
        $('#blog-tab').addClass('current-tab');


    });

    $('#project-tab a').click(function () {

        $('#blog-showcase').fadeOut(400, function () {
            $('#portfolio-showcase').fadeIn(400);
        })

        $('#blog-tab').removeClass('current-tab');
        $('#project-tab').addClass('current-tab');

    });

    /* -------------------------------- Image Preloader and Image Overlay ------------------------------ */

    function mo_imageOverlay() {

        $("#content .image-info").hide();

        $(".hfeed .post .image-frame, .image-grid .image-frame").hover(function () {
            $(this).find(".image-info").fadeTo(400, 1);
        }, function () {
            $(this).find(".image-info").fadeTo(400, 0);
        });
    }


    $.fn.preloader = function (prefs) {

        var defaults = {
            delayBetweenLoads: 200,
            parentWrapper: 'a',
            imageSelector: 'img',
            checkTimer: 300,
            onComplete: function () {
            },
            onImageLoad: function (image) {
            },
            fadeIn: 500,
            refresh: false
        };

        // variables declaration and precaching images and parent container
        var options = $.extend(defaults, prefs);

        var images = $(this).find(options.imageSelector);

        // If just refreshing the images without reload page (e.g., while quicksanding in sorted/filtered portfolio pages)
        if (options.refresh) {
            images.each(function () {
                $(this).css({ 'visibility': 'visible'}).animate(
                    { opacity: 1},
                    100,
                    function () {
                        $(this).closest(options.parentWrapper).removeClass("preloader");
                    });
                options.onImageLoad($(this));
            });
            options.onComplete(); // Callback function to be executed after loading all images
            return; // Done with preload once refresh is complete
        }
        // If refresh is not the chosen option
        var timer, imageChecked = [], delayBeforeLoad = options.delayBetweenLoads;
        var counter = 0, i = 0;

        var preloadInit = function () {

            // Call the function every timer duration specified
            timer = setInterval(function () {

                if (counter >= imageChecked.length) // if done with all images
                {
                    clearInterval(timer);
                    options.onComplete(); // Callback function to be executed after loading all images
                    return;
                }

                for (i = 0; i < images.length; i++) {
                    if (images[i].complete == true) {
                        if (imageChecked[i] == false) {
                            imageChecked[i] = true;
                            options.onImageLoad(images[i]); // Callback function to be executed after each image load
                            counter++;

                            delayBeforeLoad = delayBeforeLoad + options.delayBetweenLoads; //Make the image wait for this much time before load
                        }

                        $(images[i]).css("visibility", "visible").delay(delayBeforeLoad).animate(
                            { opacity: 1},
                            options.fadeIn,
                            function () {
                                $(this).closest(options.parentWrapper).removeClass("preloader");
                            });
                    }
                }
            }, options.checkTimer)
        };

        images.each(function () {
            imageChecked[i++] = false;
        });
        images = $.makeArray(images);

        preloadInit();

    }


    /* -------------------------------- Image Preloader ------------------------------ */

    function mo_imagePreload(refresh) {
        // Works in IE but a bit unpredictable and hence disabling it
        if (jQuery.browser.msie && parseInt(jQuery.browser.version, 10) < 8) {
            $('#content').find('.image-frame img').css({ visibility: 'visible' });
            mo_imageOverlay();
            return;
        } else {
            $("#content").preloader({
                delayBetweenLoads: 300,
                fadeIn: 300,
                checkTimer: 300,
                parentWrapper: '.image-area',
                imageSelector: '.image-frame img',
                onComplete: function () {
                    mo_imageOverlay();
                },
                refresh: refresh
            });
        }
    }

    mo_imagePreload(false);

    /* ------------------- Scroll Effects ----------------------------- */

    function mo_scroll_effects() {
        if (typeof $().waypoint === 'undefined')
            return;

        $('#service-stats').waypoint(function (direction) {
            setTimeout(function () {
                $('#service-stats').addClass("anim");
            }, 0);
        }, { offset: function () {
            var window_height = $.waypoints('viewportHeight');
            var element_height = $(this).outerHeight();
            return window_height - (element_height - 100) /* Increase pixels to account for 100px bottom padding */;
        },
            triggerOnce: true});

        $('#pricing-action').waypoint(function (direction) {
            setTimeout(function () {
                $('#pricing-action .pointing-arrow img').addClass("animated fadeInRightBig");
            }, 0);
        }, { offset: function () {
            var window_height = $.waypoints('viewportHeight');
            var element_height = $(this).outerHeight();
            return window_height - (element_height - 100) /* Increase pixels to account for 100px bottom padding */;
        },
            triggerOnce: true});

        $('#marketing-policy').waypoint(function (direction) {
            setTimeout(function () {
                $('#marketing-policy .symmetric-left h3').addClass("animated fadeInLeft");
            }, 0);
            setTimeout(function () {
                $('#marketing-policy .symmetric-right h3').addClass("animated fadeInRight");
            }, 400); // delay the second effect
        }, { offset: function () {
            var window_height = $.waypoints('viewportHeight');
            var element_height = $(this).outerHeight();
            return window_height - (element_height - 100) /* Increase pixels to account for 100px bottom padding */;
        },
            triggerOnce: true});

        $('#business-growth').waypoint(function (direction) {
            setTimeout(function () {
                $('#business-growth img.video-pointer').addClass("animated fadeInLeft");
            }, 0);
        }, { offset: function () {
            var window_height = $.waypoints('viewportHeight');
            var element_height = $(this).outerHeight();
            return window_height - (element_height - 100) /* Increase pixels to account for 100px bottom padding */;
        },
            triggerOnce: true});

    }

    if (!disable_animations_on_page)
        mo_scroll_effects();

    function mo_smooth_page_load_effect() {
        $('#before-content-area, #custom-before-content-area').addClass('animated fadeInLeft delay1');
        $('#content').addClass('animated fadeIn delay2');
        $('.first-segment .heading2, .first-segment .heading2-ext, .first-segment .heading1').addClass('animated fadeInLeft delay3');
        $('.sidebar-right-nav, sidebar-left-nav').addClass('animated fadeIn delay3');
    }

    if (!disable_smooth_page_load)
        mo_smooth_page_load_effect();

    /* -------------------------------- PrettyPhoto Lightbox --------------------------*/

    function mo_prettyPhoto() {

        if (typeof $().prettyPhoto === 'undefined')
            return;

        var theme_selected = 'pp_default';

        $("a[rel^='prettyPhoto']").prettyPhoto({
            "theme": theme_selected, /* light_rounded / dark_rounded / light_square / dark_square / facebook */
            social_tools: false
        });

    }

    mo_prettyPhoto();

    /* ------------------- Tabs and Accordions ------------------------ */

    $("ul.tabs").tabs(".pane");

    $(".accordion").tabs("div.pane", {
        tabs: 'div.tab',
        effect: 'slide',
        initialIndex: 0
    });

    /* ------------------- Back to Top ------------------------ */

    $(".back-to-top").click(function (e) {
        $('html,body').animate({
            scrollTop: 0
        }, 600);
        e.preventDefault();
    });


    /* ------------------- Toggle ------------------------ */

    function toggle_state(toggle_element) {
        active_class = 'active-toggle';

        // close all others first
        toggle_element.siblings().removeClass(active_class);
        toggle_element.siblings().find('.toggle-content').slideUp("fast");

        current_content = toggle_element.find('.toggle-content');

        if (toggle_element.hasClass(active_class)) {
            toggle_element.removeClass(active_class);
            current_content.slideUp("fast");
        }
        else {
            toggle_element.addClass(active_class);
            current_content.slideDown("fast");
        }
    }

    $(".toggle-label").toggle(
        function () {
            toggle_state($(this).parent());
        },
        function () {
            toggle_state($(this).parent());
        }
    );

    /* ------------------- Contact Form Validation ------------------------ */

    $(".contact-form").validate({
        rules: {
            contact_name: {
                required: true,
                minlength: 5
            },
            contact_email: {
                required: true,
                email: true
            },
            contact_phone: {
                required: false,
                minlength: 5
            },
            contact_url: {
                required: false,
                url: true
            },
            human_check: {
                required: true,
                range: [13, 13]
            },
            message: {
                required: true,
                minlength: 15
            }
        },
        messages: {
            contact_name: {
                required: mo_theme.name_required,
                minlength: mo_theme.name_format
            },
            contact_email: mo_theme.email_required,
            contact_url: mo_theme.url_required,
            contact_phone: {
                minlength: mo_theme.phone_required
            },
            human_check: mo_theme.human_check_failed,
            message: {
                required: mo_theme.message_required,
                minlength: mo_theme.message_format
            }
        },
        errorClass: 'form-error',
        submitHandler: function (theForm) {
            $.post(
                theForm.action,
                $(theForm).serialize(),
                function (response) {
                    $("#feedback").html("<strong>" + mo_theme.success_message + "</strong>");
                });

        }

    });

    /*-----------------------------------------------------------------------------------*/
    /*	jQuery isotope functions and Infinite Scroll
     /*-----------------------------------------------------------------------------------*/

    $(function () {

        if (typeof $().isotope === 'undefined')
            return;

        var container = $('#portfolio-items');

        container.imagesLoaded(function () {
            $(this).isotope({
                // options
                itemSelector: '.portfolio-item',
                layoutMode: 'fitRows'
            });

            $('#portfolio-filter a').click(function (e) {
                //e.preventDefault();

                var selector = $(this).attr('data-value');
                container.isotope({ filter: selector });
            });
        })


        if (ajax_portfolio) {
            container.infinitescroll({
                    navSelector: '.pagination', // selector for the paged navigation
                    nextSelector: '.pagination .next', // selector for the NEXT link (to page 2)
                    itemSelector: '.portfolio-item', // selector for all items you'll retrieve
                    loading: {
                        finishedMsg: 'No more items  to load.',
                        img: template_dir + '/images/loader.gif'
                    }
                },
                // call Isotope as a callback
                function (newElements) {
                    mo_imagePreload(true);
                    var $newElems = $(newElements);
                    $newElems.imagesLoaded(function () {
                        container.isotope('appended', $newElems);
                    })
                    mo_prettyPhoto();
                }
            );
        }

    });

    /*-----------------------------------------------------------------------------------*/
    /*	Handle videos in responsive layout - Credit - http://css-tricks.com/NetMag/FluidWidthVideo/Article-FluidWidthVideo.php
     /*-----------------------------------------------------------------------------------*/

    $("#container").fitVids();

    // Take care of maps too - https://github.com/davatron5000/FitVids.js - customSelector option
    $("#content").fitVids({ customSelector: "iframe[src^='http://maps.google.com/']"});

    if (typeof twitter_id != 'undefined') {
        jQuery('#twitter').jtwt({
            username: twitter_id,
            count: tweet_count,
            image_size: 32,
            loader_text: 'Loading Tweets'
        });
    }


})
;

