#!/bin/sh
#########################################################
# Autor João Felipe Souza          12-11-2004
# qgfelipe@yahoo.com.br, felipejfs@bol.com.br
# Agradecimentos ao SONGA, sempre bem vindo ao Pós 1821.
#########################################################
 
if [ "$#" != 2 ]
then
   echo "Uso: $0 <extensao_atual> <extensao_nova>"
   echo "Exemplo: $0 cdda.mp3 mp3"
   exit
fi
 
for file in *.$1
do
   novo=`echo $file | cut -d. -f1`
   mv $file $novo.$2
done
 
exit 0

